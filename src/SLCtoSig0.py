# -*- coding: utf-8 -*-

'''
Created on Tue Apr 10 2018

@author: plousser
'''
from osgeo import gdal
from gdalconst import GA_ReadOnly
import numpy as np


### pour lire le fichier de param
from xml.dom.minidom import parse
from datetime import datetime

### pour mes ajouts dans le code
import os, os.path, optparse, sys
import re
import glob




### pour mes problèmes de 
#TMPDIR='E:/'
class get_xml_info:
    'This class reads and manipulates the input data parameters from the corresponding *.xml file.'
    
    def __init__(self, xmlFile):
        'Reading of the *.xml file.'
        # Read the file:
        try:
            xmldoc = parse(xmlFile)
        except:
            exit('Error while reading ' + xmlFile + '\nEither the file is corrupted or does not exist.')
               
        try:
            # Read the parameters of the campaigne:
            self.forest_type = xmldoc.getElementsByTagName('Scenes')[0].attributes['forest_type'].value
            self.inc_file = xmldoc.getElementsByTagName('Scenes')[0].attributes['inc_file'].value == 'true'
#            self.freq = float(xmldoc.getElementsByTagName('Scenes')[0].attributes['frequency'].value)
             
            # Read the parameters of the scenes:
            scenes = [str(s.attributes['name'].value) for s in xmldoc.getElementsByTagName('Scene')]
            heading = [float(s.attributes['heading'].value) for s in xmldoc.getElementsByTagName('Scene')]
            z_flight = [float(s.attributes['z_flight'].value) for s in xmldoc.getElementsByTagName('Scene')]
            z_terrain = [float(s.attributes['z_terrain'].value) for s in xmldoc.getElementsByTagName('Scene')]
            SLR_start = [float(s.attributes['SLR_start'].value) for s in xmldoc.getElementsByTagName('Scene')]
            pixel_spacing = [float(s.attributes['pixel_spacing'].value) for s in xmldoc.getElementsByTagName('Scene')]
            surface_resol = [float(s.attributes['surface_resol'].value) for s in xmldoc.getElementsByTagName('Scene')]
            GRD_resol = [float(s.attributes['GRD_resol'].value) for s in xmldoc.getElementsByTagName('Scene')]
#            date = [datetime.strptime(s.attributes['date'].value, "%d/%m/%Y %H:%M") for s in xmldoc.getElementsByTagName('Scene')]
#            dem = [str(s.attributes['dem'].value) for s in xmldoc.getElementsByTagName('Scene')]
#            master = [str(s.attributes['master'].value) for s in xmldoc.getElementsByTagName('Scene')]
#            
            self.sceneslist=scenes
            # Create dictionnaries:
            self.heading = dict(zip(scenes, heading))
            self.z_flight = dict(zip(scenes, z_flight))
            self.z_terrain = dict(zip(scenes, z_terrain))
            self.SLR_start = dict(zip(scenes, SLR_start))
            self.pixel_spacing = dict(zip(scenes, pixel_spacing))
            self.surface_resol = dict(zip(scenes, surface_resol))
            self.GRD_resol = dict(zip(scenes, GRD_resol))
#            self.date = dict(zip(scenes, date))
#            self.dem = dict(zip(scenes, dem))
#            self.master = dict(zip(scenes, master))
#    
        except:
            exit('Error while reading ' + xmlFile + '\nThe file may be not correctly formated.')
 
 #I add a new comment to show modification
print('Hello World')

def prog1(inputfile,paramfile,outputfile):

    print('Open the file')
    # Open input image in slant range geometry:
    if os.path.exists(inputfile):
        input_image_driver = gdal.Open(inputfile, GA_ReadOnly)
        input_image = input_image_driver.ReadAsArray()
    else : 
        print('The input file does not exist in the directory')
    ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####

    """ get the campaign name"""
    inputfilename=inputfile.split('/')[len(inputfile.split('/'))-1]
    if len(inputfilename.split('_')) > 4:
    
        campaign = (inputfilename.split('_')[0]+'_'+inputfilename.split('_')[1])
        print(campaign)
        scene = inputfilename.split('_')[2]
        level = inputfilename.split('_')[3]
        polar = inputfilename.split('_')[4]
    else :
        campaign, scene, level, polar = inputfilename.split('_')
    print(campaign, scene, level, polar)
    print('Get the infos of the parameter file')
    """ get the info of the xml_file """
    xmlfile = paramfile
    xml_info=get_xml_info(xmlfile)
    
    
    if campaign in ['indrex2', 'biosar1', 'biosar2', 'afrisar_onera']:
        # Open incidence angle map in slant range geometry:
        print("Opening the incidence angles file")
        incFilename=inputfile.replace('.tiff','_inc.tiff')
        theta_inc_map_driver = gdal.Open(incFilename, GA_ReadOnly)
        theta_inc_map = theta_inc_map_driver.ReadAsArray()
    else:
        # If not provided, compute it:
        print("Compute the incidence angle for the scene ")
        theta_inc_map = np.arccos((xml_info.z_flight[scene] - xml_info.z_terrain[scene]) / (xml_info.SLR_start[scene] + np.mgrid[:input_image_driver.RasterYSize, :input_image_driver.RasterXSize][1] * xml_info.pixel_spacing[scene]))

    # Example of computation of Sigma0 (natural) as given in the corresponding campaign reports:
    print("Sigma0 calculation")
    if campaign in ['indrex2', 'biosar1', 'biosar2', 'afrisar_dlr']: # For indrex2, biosar1, biosar2 and afrisar_dlr:
        sigma0 = np.absolute(input_image)**2 * np.sin(theta_inc_map)
    elif campaign in ['tropisar', 'biosar3', 'afrisar_onera']: # For tropisar, biosar3 and afrisar_onera:
        sigma0 = np.absolute(input_image)**2 * np.sin(theta_inc_map) / xml_info.surface_resol[scene]
    
    # Filter bad data:
    sigma0[sigma0 <= 0] = np.NaN
    
    
    ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
    # Save output image in slant range geometry:
    print("Saving the outputfile " + outputfile)
    outdriver = gdal.GetDriverByName('GTiff')
    output_image_driver = outdriver.Create(outputfile, input_image_driver.RasterXSize, input_image_driver.RasterYSize, 1, gdal.GDT_Float32)
    output_image_driver.GetRasterBand(1).WriteArray(sigma0)
    
    # Close data sets drivers:
    input_image_driver = None
    theta_inc_map_driver = None
    output_image_driver = None
    outdriver=None
    # Delete datasets
    input_image=None
    theta_inc_map=None
    sigma0=None

    
      
if __name__ == '__main__':
    from properties.p import Property
    import quicklook_raster
    if os.path.isfile('/projects/slctosig0/conf/configuration.properties'):
        #print('toto')
        configfile='/projects/slctosig0/conf/configuration.properties'
    
        prop=Property()
        prop_dict=prop.load_property_files(configfile)
    
        prog1(prop_dict['inputfile'], prop_dict['xmlfile'],prop_dict['outputfile'])
        quicklook_raster.main(prop_dict['outputfile'])
    
